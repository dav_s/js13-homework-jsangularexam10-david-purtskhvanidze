create schema posts_david_js13_database collate utf8_general_ci;
use posts_david_js13_database;

create table news
(
    id          int auto_increment primary key,
    title       varchar(255)       not null,
    text        text               not null,
    image       varchar(32)        null,
    post_data   varchar(255)       null
);

create table comments
(
    id          int auto_increment primary key,
    news_id     int                null,
    author      varchar(255)       not null,
    text        text               not null,
    constraint comments_news_id_fk
        foreign key (news_id) references news (id)
            on update cascade on delete set null

);



