const express = require('express');
const config = require('../config');
const db = require('../mySqlDb');

const router = express.Router();

router.get('/', async (req, res, next) => {
    try {
        let query = 'SELECT * FROM comments';

        if (req.query.filter === 'image') {
            query += ' WHERE image IS NOT NULL';
        }

        let [comments] = await db.getConnection().execute(query);

        return res.send(comments);
    } catch (e) {
        next(e);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        const [comments] = await db.getConnection().execute('SELECT * FROM comments WHERE news_id = ?', [req.params.id]);

        const comment = comments[0];

        if (!comment) {
            return res.status(404).send({message: 'Not found'});
        }

        return res.send(comment);
    } catch (e) {
        next(e);
    }
});

router.get('/delete/:id', async (req, res, next) => {
    try {
        await db.getConnection().execute('DELETE FROM comments WHERE id = ?', [req.params.id]);

        return res.send({message: 'Comment with id=' + req.params.id +' is deleted'});
    } catch (e) {
        next(e);
    }
});

router.post('/', async (req, res, next) => {
    try {
        if (!req.body.news_id || !req.body.text) {
            return res.status(400).send({message: 'News ID and text are required'});
        }

        const comment = {
            news_id: req.body.news_id,
            author: req.body.author,
            text: req.body.text,
        };

        let query = 'INSERT INTO comments (news_id, author, text) VALUES (?, ?, ?)';

        const [results] = await db.getConnection().execute(query, [
            comment.news_id,
            comment.author,
            comment.text,
        ]);

        comment.id = results.insertId;

        return res.send(comment);
    } catch (e) {
        next(e);
    }
});

module.exports = router;