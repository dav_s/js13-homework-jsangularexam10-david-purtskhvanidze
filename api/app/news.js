const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../config');
const db = require('../mySqlDb');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
    try {
        let query = 'SELECT id, title, image, post_data FROM news';

        if (req.query.filter === 'image') {
            query += ' WHERE image IS NOT NULL';
        }

        let [news] = await db.getConnection().execute(query);

        return res.send(news);
    } catch (e) {
        next(e);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        const [news] = await db.getConnection().execute('SELECT * FROM news WHERE id = ?', [req.params.id]);

        const news_item = news[0];

        if (!news_item) {
            return res.status(404).send({message: 'Not found'});
        }

        return res.send(news_item);
    } catch (e) {
        next(e);
    }
});

router.get('/delete/:id', async (req, res, next) => {
    try {
        await db.getConnection().execute('DELETE FROM news WHERE id = ?', [req.params.id]);

        return res.send({message: 'News with id=' + req.params.id +' is deleted'});
    } catch (e) {
        next(e);
    }
});

router.post('/', upload.single('image'), async (req, res, next) => {
    try {
        if (!req.body.title || !req.body.text) {
            return res.status(400).send({message: 'Title and text are required'});
        }

        const news_item = {
            title: req.body.title,
            text: req.body.text,
            image: null,
            post_data: null,
        };

        news_item.post_data = new Date().toISOString();

        if (req.file) {
            news_item.image = req.file.filename;
        }

        let query = 'INSERT INTO news (title, text, image, post_data) VALUES (?, ?, ?, ?)';

        const [results] = await db.getConnection().execute(query, [
            news_item.title,
            news_item.text,
            news_item.image,
            news_item.post_data
        ]);

        news_item.id = results.insertId;

        return res.send(news_item);
    } catch (e) {
        next(e);
    }
});

module.exports = router;