export class Comment {
  constructor(
    public id: string,
    public news_id: string,
    public author: string,
    public text: string,
  ) {}
}

export interface CommentData {
  [key: string]: any;
  news_id: string;
  author: string;
  text: string;
}
