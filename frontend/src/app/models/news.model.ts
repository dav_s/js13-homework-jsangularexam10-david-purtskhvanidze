export class News {
  constructor(
    public id: string,
    public title: string,
    public text: number,
    public image: string,
    public post_data: string,
  ) {}
}

export interface NewsData {
  [key: string]: any;
  title: string;
  text: string;
  image: File | null;
  post_data: string;
}
