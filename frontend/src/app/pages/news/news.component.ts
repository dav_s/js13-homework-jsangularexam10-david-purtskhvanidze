import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { News } from '../../models/news.model';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-posts',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.sass']
})
export class NewsComponent implements OnInit {
  news: News[] = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.getNews().subscribe({
      next: news => {
        this.news = news;
      },
      error: error => {
        console.error('something');
      }
    });
  }

  getNews() {
    return this.http.get<News[]>(environment.apiUrl + '/news').pipe(
      map(response => {
        return response.map(newsData => {
          return new News(
            newsData.id,
            newsData.title,
            newsData.text,
            newsData.image,
            newsData.post_data,
          );
        });
      })
    );
  }

}
