import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { NewsData } from '../../models/news.model';


@Component({
  selector: 'app-add-new',
  templateUrl: './add-new.component.html',
  styleUrls: ['./add-new.component.sass']
})
export class AddNewComponent implements OnInit {

  @ViewChild('f') form!: NgForm;

  constructor(
    private router: Router,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
  }

  createNews(newsData: NewsData) {
    const formData = new FormData();

    Object.keys(newsData).forEach(key => {
      if (newsData[key] !== null) {
        formData.append(key, newsData[key]);
      }
    });

    return this.http.post(environment.apiUrl + '/news', formData);
  }

  onSubmit() {
    const newsData: NewsData = this.form.value;
    this.createNews(newsData).subscribe(() => {
      void this.router.navigate(['/']);
    });
  }
}
