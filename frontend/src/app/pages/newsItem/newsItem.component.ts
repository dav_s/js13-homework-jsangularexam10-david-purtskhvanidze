import { Component, OnInit, ViewChild } from '@angular/core';
import { Comment, CommentData } from '../../models/comment.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-post',
  templateUrl: './newsItem.component.html',
  styleUrls: ['./newsItem.component.sass']
})
export class NewsItemComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  comments: Comment[] = [];
  newsId = '';

  constructor(
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
    ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.newsId = params['id'];
    });

    this.getCommentsByNewsId(this.newsId).subscribe({
      next: comments => {
        this.comments = comments;
      },
      error: error => {
        console.error('something');
      }
    });
  }

  getCommentsByNewsId(newsId: string) {
    return this.http.get<Comment[]>(environment.apiUrl + `/comments/${newsId}`).pipe(
      map(response => {
        return response.map(commentData => {
          return new Comment(
            commentData.id,
            commentData.news_id,
            commentData.author,
            commentData.text,
          );
        });
      })
    );
  }

  addComment(commentData: CommentData) {
    const formData = new FormData();

    Object.keys(commentData).forEach(key => {
      if (commentData[key] !== null) {
        formData.append(key, commentData[key]);
      }
    });

    return this.http.post(environment.apiUrl + '/comments', formData);
  }

  onSubmit() {
    const commentData: CommentData = this.form.value;
    this.addComment(commentData).subscribe();
  }

}
