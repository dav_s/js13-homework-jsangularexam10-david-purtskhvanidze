import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewsComponent } from './pages/news/news.component';
import { AddNewComponent } from './pages/add-new/add-new.component';
import { NotFoundComponent } from './not-found.component';
import { NewsItemComponent } from './pages/newsItem/newsItem.component';

const routes: Routes = [
  {path: '', component: NewsComponent},
  {path: 'news/:id', component: NewsItemComponent},
  {path: 'add-new', component: AddNewComponent},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
